﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// 参考
// drag and drop cell from datagridview to another
// https://stackoverflow.com/questions/21131157/drag-and-drop-cell-from-datagridview-to-another

namespace CSharpDataGridViewDragAndDrop2
{
    public partial class Form1 : Form
    {
        /**
         * @brief コンストラクタ
         */
        public Form1()
        {
            InitializeComponent();
        }

        /**
         * @brief フォームがロードされた時に呼び出されます。
         * 
         * @param [in] sender フォーム
         * @param [in] e イベント
         */
        private void Form1_Load(object sender, EventArgs e)
        {
            // サンプルデータを設定します。
            // ドロップ元のDataGridView
            dataGridView1.ColumnCount = 3;
            dataGridView1.Columns[0].HeaderText = "値A";
            dataGridView1.Columns[1].HeaderText = "値B";
            dataGridView1.Columns[2].HeaderText = "値C";
            dataGridView1.Rows.Add("A-1", "B-1", "C-1");
            dataGridView1.Rows.Add("A-2", "B-2", "C-2");
            dataGridView1.Rows.Add("A-3", "B-3", "C-3");

            // 選択モードをセルに設定します。
            dataGridView1.SelectionMode = DataGridViewSelectionMode.CellSelect;

            // ドロップ先のDataGridView
            dataGridView2.ColumnCount = 3;
            dataGridView2.Columns[0].HeaderText = "値X";
            dataGridView2.Columns[1].HeaderText = "値Y";
            dataGridView2.Columns[2].HeaderText = "値Z";
            dataGridView2.Rows.Add("X-1", "Y-1", "Z-1");
            dataGridView2.Rows.Add("X-2", "Y-2", "Z-2");
            dataGridView2.Rows.Add("X-3", "Y-3", "Z-3");

            // ドロップを許可します。
            dataGridView2.AllowDrop = true;
        }

        /**
         * @brief マウスのボタンが押下された時に呼び出されます。
         * 
         * @param [in] sender マウスのボタン
         * @param [in] e イベント
         */
        private void dataGridView1_MouseDown(object sender, MouseEventArgs e)
        {
            // マウスの左ボタン以外が押下された場合
            if (e.Button != MouseButtons.Left)
            {
                // 何もしません。
                return;
            }

            // マウスが押下された座標から、dataGridViewの位置情報を取得します。
            var hitTest = dataGridView1.HitTest(e.X, e.Y);
            int rowIndex = hitTest.RowIndex;
            int columnIndex = hitTest.ColumnIndex;

            // ヘッダ列、ヘッダ行の場合
            if ((rowIndex < 0) || (columnIndex < 0))
            {
                // 何もしません。
                return;
            }

            // ドロップ元(dataGridView1)のセルの内容を取得します。
            var text = (string)dataGridView1.Rows[rowIndex].Cells[columnIndex].Value;

            // セルの内容が取得できない場合
            if (text == null)
            {
                // 何もしません。
                return;
            }

            // ドラッグアンドドロップ動作を開始します。
            dataGridView1.DoDragDrop(text, DragDropEffects.Copy);
        }

        /**
         * @brief ドラッグされた場合呼び出されます。
         * 
         * @param [in] sender 対象のコントロール
         * @parma [in] e イベント
         */
        private void dataGridView2_DragEnter(object sender, DragEventArgs e)
        {
            // ドラッグアンドドロップのドロップ効果をコピーに設定します。
            e.Effect = DragDropEffects.Copy;
        }

        /**
         * @brief ドラッグアンドドロップでドロップされた時に呼び出されます。
         * 
         * @param [in] sender 対象のコントロール
         * @parma [in] e イベント
         */
        private void dataGridView2_DragDrop(object sender, DragEventArgs e)
        {
            // ドロップ先(dataGridView2)のクライアント位置からDataGridViewの位置情報を取得します。
            var value = e.Data.GetData(typeof(string)) as string;
            var point = dataGridView2.PointToClient(new Point(e.X, e.Y));
            var hitTest = dataGridView2.HitTest(point.X, point.Y);
            int columnIndex = hitTest.ColumnIndex;
            int rowIndex = hitTest.RowIndex;

            // ヘッダ列、ヘッダ行ではない場合
            if ((columnIndex != -1) && (rowIndex != -1))
            {
                // ドロップ元(dataGridView1)のセルの内容を、
                // ドロップ先(dataGridView2)のセルにコピーします。
                dataGridView2[columnIndex, rowIndex].Value = value;
            }
        }
    }
}
